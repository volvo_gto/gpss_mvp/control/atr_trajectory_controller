use r2r;
use r2r::atr_path_msgs::msg::PathWithDTime;
use r2r::geometry_msgs::msg::Twist;
use tokio::sync::mpsc;
use futures::stream::StreamExt;
use serde_json::Value;
use serde::{Deserialize, Serialize};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let atr_id = 1;

    let twist_msg = Twist::default();
    println!("twist: {:?}", twist_msg);
    let path_msg = PathWithDTime::default();
    println!("path: {:?}", path_msg);

    let ctx = r2r::Context::create()?;
    let mut node = r2r::Node::create(ctx, "testnode_controller", "")?;

    let handle = tokio::task::spawn_blocking(move || loop {
        node.spin_once(std::time::Duration::from_millis(100));
    });

    // await spin task (will never finish)
    handle.await?;

    Ok(())
}
